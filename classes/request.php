<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * request class
 **/
class Request {
    
    public static $method = 'GET';
    public static $protocol = 'http';
    public static $referrer;
    public static $user_agent = '';
    public static $client_ip = '0.0.0.0';
    public static $is_ajax = FALSE;
    public $uri;

    public static $instance;
    public static $current;
    public $controller;
    public $action;
    protected $_params = array();

    function __construct() {
        if (php_sapi_name() == 'cli') {
            $argv = $_SERVER['argv'];
            array_shift($argv);
            $uri = array_pop($argv);
        } else {
            $uri = $this->detect_uri();
        }
        
        //Crude parsing
        $combine = parse_url($uri);
        $c = explode('/', $combine['path']);
        //hardcode for folder
        array_shift($c);

        $this->controller = empty($c[0]) ? 'welcome' : $c[0];
        $this->action = empty($c[1]) ? 'index' : $c[1];
        $this->_params['id'] = empty($c[2]) ? null : $c[2];
        $params = array();
        $this->_params = array_merge($this->_params, $this->make_request(), $_GET, $params);
        //end crude parsing
    }

    /**
     * @param bool $uri
     * @return Request
     */

    public static function me($uri = TRUE) {
    
        if (isset($_SERVER['REQUEST_METHOD'])) {
            // Use the server request method
            Request::$method = $_SERVER['REQUEST_METHOD'];
        }

        if (!empty($_SERVER['HTTPS']) AND filter_var($_SERVER['HTTPS'], FILTER_VALIDATE_BOOLEAN)) {
            // This request is secure
            Request::$protocol = 'https';
        }

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
            // This request is an AJAX request
            Request::$is_ajax = TRUE;
        }

        if (isset($_SERVER['HTTP_REFERER'])) {
            // There is a referrer for this request
            Request::$referrer = $_SERVER['HTTP_REFERER'];
        }

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            // Set the client user agent
            Request::$user_agent = $_SERVER['HTTP_USER_AGENT'];
        }

        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            // Use the forwarded IP address, typically set when the
            // client is using a proxy server.
            Request::$client_ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        elseif (isset($_SERVER['HTTP_CLIENT_IP']))
        {
            // Use the forwarded IP address, typically set when the
            // client is using a proxy server.
            Request::$client_ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (isset($_SERVER['REMOTE_ADDR']))
        {
            // The remote IP address
            Request::$client_ip = $_SERVER['REMOTE_ADDR'];
        }

        if (Request::$method !== 'GET' AND Request::$method !== 'POST') {
            // Methods besides GET and POST do not properly parse the form-encoded
            // query string into the $_POST array, so we overload it manually.
            parse_str(file_get_contents('php://input'), $_POST);
        }

        if ($uri === TRUE) {
            $uri = Request::detect_uri();
        }

        if (!isset(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function execute() {
        $postfix = "_controller";
        $previous = Request::$current; //save request
        Request::$current = $this;
        try {
            $class = new ReflectionClass($this->controller.$postfix);
            $controller = $class->newInstance($this);
            $class->getMethod('before')->invoke($controller);
            $action = empty($this->action) ? Route::default_action : $this->action;
            $class->getMethod('action_'.$this->action)->invokeArgs($controller, $this->_params);
            $class->getMethod('after')->invoke($controller);
        } catch (Exception $e) {
            Request::$current = $previous; //возвращаем реквест обратно
            //сюда можно поставить кастомные exceptions
            throw $e;
        }
    }

    /**
     * @return string
     * @throws Exception
     */

    public static function detect_uri() {

        if (isset($_SERVER["REQUEST_URI"])) {
            $uri = $_SERVER["REQUEST_URI"];
            $uri = rawurldecode($uri);
        } elseif (isset($_SERVER["PHP_SELF"])) {
            $uri = $_SERVER["PHP_SELF"];
        } elseif (isset($_SERVER["REDIRECT_URL"])) {
            $uri = $_SERVER["REDIRECT_URL"];
        } else {
            throw new Exception('Unable to detect uri. Are we even using server?');
        }
        return $uri;
    }

    public function params() {
        return $this->_params;
    }

    public function param($key = NULL) {
        if (!is_null($key) && isset($this->_params[$key])) {
            return $this->_params[$key];
        }
        return FALSE;
    }

    public function redirect($uri) {
        header('Location: '.$uri);
        exit();
    }
    
    protected function make_request() {
        $result = array();
        foreach (explode("&", file_get_contents("php://input")) as $pair) {
            if ($pair) {
                list($key, $value) = explode("=", $pair);
                $name = urldecode($key);
                $value = urldecode($value);
                if (!isset($_POST[$name]))
                    $_POST[$name] = $value;
                $result[$name] = $value;
            }
        }
        return $result;
    }

}
?>
