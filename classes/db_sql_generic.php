<?php
abstract class DB_Sql_Generic extends DB {

    //this method if for selects
    //returns dataset
    //@todo!!! QUERY COMPILATION - make sure it compiles in right order
    //@todo!!! join!!!
    public function query($as_array=false) {
        $results = $this->db->query($this->query);
        $i = 0;
        $response = array();
        foreach ($results as $result) {
            $response[] = $result;
        }
        if (count($response) > 1) {
            $result = $response;
        }elseif (count($response) == 0) {
            return null;
        } else {
            if ($as_array) {
                $result = $response;
            } else {
                $result = $response[0];
            }
        }
        $this->query = NULL;
        return $result;
    }

    //this is for update/delete/drop etc, 
    //doesnt return dataset
    public function exec($query = NULL) {
        if (is_null($query)) {
            $query = $this->query;
        }
        $result = $this->db->exec($query);
        $this->query = NULL; //сбрасываем текущий запрос
        return $result;
    }
    //escape dangerous string
    public function quote($str) {
        return $this->db->quote($str);
    }

    public static function shutdown() {
        return $this->db = NULL;
    }

    public function select(Model $model, $fields = null) {

        $this->query = 'select ';

        if (is_null($fields)) {
            $fields = array_keys($model->fields);
        }
        
        $this->query .= implode(',', $fields);

        $this->query .= ' from '.$model->table;

        return $this;

    }

    public function where($conditions) {
        $this->query .= ' where '.$conditions;
        return $this;
    }

    public function groupBy($array) {
        $this->query .= ' group by '.implode($array, ',');
        return $this;
    }

    public function having($conditions) {
        $this->query .= ' having '.$conditions;
        return $this;
    }

    public function join($arr) {
        //comment
        if (is_array($arr)) {
            foreach ($arr as $str) {
                $this->query .= ' join '.$str;
            }
        } else {
            $this->query .= ' join '.$str;
        }
        return $this;
    }

    public function orderBy($str) {
        $this->query .= ' order by '.$str;
        return $this;
    }

    public function limit($count) {
        if (intval($count)) {
            $this->query .= ' limit '.intval($count);
        }
        return $this;
    }

    public function offset($count) {
        if (intval($count)) {
            $this->query .= ' offset '.intval($count);
        }
        return $this;
    }

    public function raw($query_string) {
        $this->query = $query_string;
    }
    
    public function to_string() {
        return $this->query;
    }
}
