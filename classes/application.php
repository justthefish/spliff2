<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * Application class
 **/
class Application {

    public static $instance;

    public function __construct() {
        // code...
    }

    public static function me() {
        if (!isset(Application::$instance)) {
            Application::$instance = new Application();
        }
        return Application::$instance;
    }

    public function run() {
        $request = Request::me();
        try {
            $request->execute();

        } catch (ReflectionException $e) { 

            header('HTTP/1.0 404 Not Found');
            //@todo include page
            $request->response = "404 - Not found";

        } catch (Exception $e) {
            throw $e;
        }
        echo $request->response;
    }
        
}
?>
