<?php
class DB_Mysql extends DB_SQL_Generic {

    public function __construct() {
        $attr = parse_url(MYSQL_CONNECTION_STRING);
        $this->db = new PDO($attr['scheme'].':host='.$attr['host'].';dbname='.substr($attr['path'],1), $attr['user'], $attr['pass']);
        //$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
    }

}
?>
