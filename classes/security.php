<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * security calss
 **/
class Security {

    public static function gen_uuid($len=8) {

        $hex = md5("this_iS_tEh_s4lt!" . uniqid("", true));

        $pack = pack('H*', $hex);
        $tmp =  base64_encode($pack);

        $uid = preg_replace("/[^A-Za-z0-9]/u", "", $tmp);

        $len = max(4, min(128, $len));

        while (strlen($uid) < $len)
            $uid .= self::gen_uuid(22);

        return substr($uid, 0, $len);
    }

    public static function gen_key($login) {
        return md5(substr($login, 0, 17).substr(md5($login),-14, 21).'swirly curly br4ce5');
    }
}
