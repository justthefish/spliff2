<?php
/**
 * Created by VIM
 * @user: thefish
 * @author: anton.gurov@gmail.com
 * Validation singletion
 **/
class Validation {
    
    public static $instance;

    public function __construct() {
    }

    //singleton
    public static function me() {
        if (!isset(Validation::$instance)) {
            Validation::$instance = new Validation();
        }

        return Validation::$instance;
    }

    public function not_empty($field, $value) {
        if (empty($value)) {
            return "$field must not be empty!";
        } else {
            return TRUE;
        }
    }

    public function email($field, $value) {
        $pattern = '/^[a-zA-Z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+(\.[a-zA-Z0-9\!\#\$\%\&\'\*\+\-\/\=\?\^\_\`\{\|\}\~]+)*@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/Ds'; 
        if (preg_match($pattern, $value)) {
            return TRUE;
        } else {
            return "$field must be a valid email!";
        }
    }
}
