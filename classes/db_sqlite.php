<?php
class DB_Sqlite extends DB_Sql_Generic {

    public function __construct() {
        $this->db = new PDO(SQLITE_CONNECTION_STRING);

        $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);

    }

}
?>
