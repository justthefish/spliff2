<?php
class DB_Mongo extends DB {

    public function __construct($server = MONGO_CONNECTION_STRING, $options = array()) {
        $this->db = new Mongo($server, $options);        
        return $this->db;
    } 
    
    public static function shutdown() {
        return $this->db = NULL;
    }
}
